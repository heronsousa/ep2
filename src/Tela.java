
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Tela extends JFrame {
    
    JLabel recursos = new JLabel("RECURSOS: ");
    JTextField qt = new JTextField();
    JMenuBar barra = new JMenuBar();
    JMenu menu = new JMenu("Jogo");
    JMenuItem item = new JMenuItem("Instruções");
    
    private int LARGURA_BOTAO = 190;
    private int ALTURA_BOTAO = 40;

    public Tela() throws IOException {
           
        Tabuleiro jogo = new Tabuleiro();
        
        setJMenuBar(barra);
        barra.add(menu);
        menu.add(item);
        
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                    java.nio.file.Path arquivo = Paths.get("arquivos/instrucoes.txt");
                try {
                    byte[] texto = Files.readAllBytes(arquivo);
                    String leitura = new String(texto);
                    JOptionPane.showMessageDialog(menu, leitura);
                } catch (IOException ex) {
                    Logger.getLogger(Tela.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
        JButton botao = new JButton("ATACAR LINHA");
        JButton botao1 = new JButton("ATACAR POSIÇÃO");
        JButton botao2 = new JButton("ATACAR COLUNA");
        JButton botao3 = new JButton("ATACAR AREA 2x2");
        JButton botao4 = new JButton("DESCOBRIR AREA 2x2");
        
        getContentPane().setLayout(new BorderLayout());
        setTitle("BATALHA NAVAL");
        
        this.add(recursos);
        this.add(qt);
        qt.setText(String.valueOf(jogo.recursos));
        this.add(botao);
        botao.addActionListener(jogo.acao);
        this.add(botao1);
        botao1.addActionListener(jogo.acao1);
        this.add(botao2);
        botao2.addActionListener(jogo.acao2);
        this.add(botao3);
        botao3.addActionListener(jogo.acao3);
        this.add(botao4);
        botao4.addActionListener(jogo.acao4);
        
        getContentPane().add("Center", jogo);
        setSize(jogo.colunas*jogo.LARGURA+LARGURA_BOTAO+40, jogo.linhas*jogo.ALTURA+80);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true);
        
        recursos.setBounds(jogo.colunas*jogo.LARGURA+20, 20, 80, ALTURA_BOTAO);
        qt.setBounds(jogo.colunas*jogo.LARGURA+95, 28, 30, 25);
        botao1.setBounds(jogo.colunas*jogo.LARGURA+20, 70, LARGURA_BOTAO, ALTURA_BOTAO);
        botao.setBounds(jogo.colunas*jogo.LARGURA+20, 120, LARGURA_BOTAO, ALTURA_BOTAO);
        botao2.setBounds(jogo.colunas*jogo.LARGURA+20, 170, LARGURA_BOTAO, ALTURA_BOTAO);
        botao3.setBounds(jogo.colunas*jogo.LARGURA+20, 220, LARGURA_BOTAO, ALTURA_BOTAO);
        botao4.setBounds(jogo.colunas*jogo.LARGURA+20, 270, LARGURA_BOTAO, ALTURA_BOTAO);

        jogo.addMouseListener(new MouseListener() {

            @Override
            public void mouseReleased(MouseEvent e) {

                int x = e.getX();
                int y = e.getY();
                int x_pos = x / jogo.LARGURA;
                int y_pos = y / jogo.ALTURA;
                jogo.setTiro(x_pos, y_pos);
            }

            public void mouseClicked(MouseEvent e) {
            }
            public void mousePressed(MouseEvent e) {
            }
            public void mouseEntered(MouseEvent e) {
            }
            public void mouseExited(MouseEvent e) {
            }
        });
    }
}
