import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;

public class Tabuleiro extends Canvas {
    
    public int linhas, colunas;
    
    private int barcos, barcosNaArea;
    
    public int recursos;
    
    public int LARGURA = 50;
    public int ALTURA = 50;
    private int MARGEM = 10;
    
    public ArrayList<String> coordenadasX = new ArrayList<>();
    public ArrayList<String> coordenadasY = new ArrayList<>();
    
    private Boolean linha = false;
    private Boolean tiro = false;
    private Boolean coluna = false;
    private Boolean area2x2 = false;
    private Boolean dArea2x2 = false;
    private Boolean nAchou = true;
    
    private int[][] Tiro = new int[colunas+15][linhas+15];
        
    public Tabuleiro() throws IOException {
        
        System.out.println("INSIRA O NO DO MAPA (Ex.: map_1.txt):");
        Scanner ler = new Scanner(System.in);
        String dir = ler.next();
        
        try {
            Scanner arquivo = new Scanner(new FileReader("mapas/" + dir));
            
            String linha1 = arquivo.nextLine();
            colunas = arquivo.nextInt();
            linhas = arquivo.nextInt();
            String linha2 = arquivo.nextLine();
            String linha3 = arquivo.nextLine();
            String linha4 = arquivo.nextLine();
            for (int i = 0; i < linhas; i++) {
                String valor = arquivo.next();
                for (int j = 0; j < valor.length(); j++) {
                    int numero = Integer.parseInt(String.valueOf(valor.charAt(j)));
                    if (numero != 0) {
                        coordenadasY.add(String.valueOf(i));
                        coordenadasX.add(String.valueOf(j));
                    }
                }
            }    
        } catch (FileNotFoundException erro) {
            System.out.println("Arquivo não encontrado!");
            System.exit(0);
        }
        
        recursos = coordenadasX.size()*3;
    }
     
    public void paint(Graphics g) {
        
        ImageIcon bomb = new ImageIcon("arquivos/frame-1.png");
        final Image agua = bomb.getImage();
        ImageIcon icon = new ImageIcon("arquivos/frame-10.png");
        final Image img = icon.getImage();
        barcos = 0;   
        barcosNaArea = 0;
        
        for (int i = 0; i < colunas; i++) {
            for (int j = 0; j < linhas; j++) {
                g.setColor(Color.BLACK);
                g.drawRect(i * ALTURA+MARGEM, j * LARGURA+MARGEM, ALTURA, LARGURA);
                g.drawImage(agua, i*ALTURA+MARGEM+1, j*LARGURA+MARGEM+1, ALTURA-1, LARGURA-1, null);
            } 
        }
        for (int i = 0; i < colunas; i++) {
            for (int j = 0; j < linhas; j++) {
                if (Tiro[j][i] == 1) {
                    g.setColor(Color.RED);
                    g.fillRect(i * ALTURA+MARGEM+1, j * LARGURA+MARGEM+1, ALTURA - 1, LARGURA - 1);
                    barcos++;
                }   
                if (Tiro[j][i] == 2) {
                    g.drawImage(img, i*ALTURA+MARGEM+1, j*LARGURA+MARGEM+1, ALTURA-1, LARGURA-1, null);
                    tiro = false;
                    coluna = false;
                    linha = false;
                    area2x2 = false;
                    dArea2x2 = false;
                }
            }
        }
        if (barcos == coordenadasX.size()) {
            JOptionPane.showMessageDialog(this, "PARABÉNS!! VOCÊ ENCONTROU TODAS AS EMBARCAÇÕES!");
        }
        
    }
    
    ActionListener acao = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            JButton botao = (JButton) ae.getSource();
            linha = true;
            tiro = false;
            dArea2x2 = false;
            coluna = false;
            area2x2 = false;
            if(recursos-linhas < 0){
                botao.setEnabled(false);
                linha = false;
            }
            else{
                recursos-=linhas;
            }    
        }
    };
    ActionListener acao1 = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            JButton botao1 = (JButton) ae.getSource();
            tiro = true;
            coluna = false;
            dArea2x2 = false;
            linha = false;
            area2x2 = false;
            if( recursos-1 < 0){
                botao1.setEnabled(false);
                tiro = false;
            }
            else{
                recursos--;
            }
        }
    };
    ActionListener acao2 = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            JButton botao2 = (JButton) ae.getSource();
            coluna = true;
            linha = false;
            area2x2 = false;
            dArea2x2 = false;
            tiro = false;
            if( recursos-colunas < 0){
                botao2.setEnabled(false);
                coluna = false;
            }
            else{
                recursos-=colunas;
            }
        }
    };
    ActionListener acao3 = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            JButton botao3 = (JButton) ae.getSource();
            area2x2 = true;
            tiro = false;
            coluna = false;
            linha = false;
            dArea2x2 = false;
            if( recursos-4 < 0){
                botao3.setEnabled(false);
                area2x2 = false;
            }
            else{
                recursos-=4;
            }
        }
    };
    ActionListener acao4 = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            JButton botao4 = (JButton) ae.getSource();
            dArea2x2 = true;
            area2x2 = false;
            tiro = false;
            coluna = false;
            linha = false;
            if( recursos-4 < 0){
                botao4.setEnabled(false);
                dArea2x2 = false;
            }
            else{
                recursos-=2;
            }
        }
    };
    
    public void setTiro(int x, int y) {
        
        int i;
        int j;
        int cont = 0;
                
        if(tiro){
            while(cont != coordenadasX.size()){
                int posX = Integer.parseInt(String.valueOf(coordenadasX.get(cont)));
                int posY = Integer.parseInt(String.valueOf(coordenadasY.get(cont)));
                if(x == posX && y == posY){
                    Tiro[y][x] = 1;
                    repaint();
                    nAchou = false;
                }
                cont++;
            }
            if(nAchou) {
                Tiro[y][x] = 2;
                repaint();    
            }
            nAchou = true;    
        }
        if (linha) {
            for (i = 0, j = y; i < colunas; i++) {
                while (cont != coordenadasX.size()) {
                    int posX = Integer.parseInt(String.valueOf(coordenadasX.get(cont)));
                    int posY = Integer.parseInt(String.valueOf(coordenadasY.get(cont)));
                    if (i == posX && j == posY) {
                        Tiro[j][i] = 1;
                        repaint();
                        nAchou = false;
                    }
                    cont++;
                }
                cont = 0;
                if (nAchou) {
                    Tiro[j][i] = 2;
                    repaint();
                }
                nAchou = true;
            }
        }
        if(coluna){
            for (i = x, j = 0; j < linhas; j++) {
                while (cont != coordenadasX.size()) {
                    int posX = Integer.parseInt(String.valueOf(coordenadasX.get(cont)));
                    int posY = Integer.parseInt(String.valueOf(coordenadasY.get(cont)));
                    if (i == posX && j == posY) {
                        Tiro[j][i] = 1;
                        repaint();
                        nAchou = false;
                    }
                    cont++;
                }
                cont = 0;
                if (nAchou) {
                    Tiro[j][i] = 2;
                    repaint();
                }
                nAchou = true;
            }
        }
        if(area2x2){
            for (i=x; i<x+2; i++){
                for(j=y; j<y+2; j++){
                    while (cont != coordenadasX.size()) {
                    int posX = Integer.parseInt(String.valueOf(coordenadasX.get(cont)));
                    int posY = Integer.parseInt(String.valueOf(coordenadasY.get(cont)));
                    if (i == posX && j == posY) {
                        Tiro[j][i] = 1;
                        repaint();
                        nAchou = false;
                    }
                    cont++;
                }
                cont = 0;
                if (nAchou) {
                    Tiro[j][i] = 2;
                    repaint();
                }
                nAchou = true;
                }
            }
        }
        if(dArea2x2){
            for (i=x; i<x+2; i++){
                for(j=y; j<y+2; j++){
                    while (cont != coordenadasX.size()) {
                    int posX = Integer.parseInt(String.valueOf(coordenadasX.get(cont)));
                    int posY = Integer.parseInt(String.valueOf(coordenadasY.get(cont)));
                    if (i == posX && j == posY) {
                        nAchou = false;
                    }
                    cont++;
                    }
                    cont = 0;
                    if (nAchou) {
                        barcosNaArea++;
                    }
                    nAchou = true;
                }
            }
            if (barcosNaArea == 4) {
                JOptionPane.showMessageDialog(this, "NÃO HÁ EMBARÇÕES NESSA AREA!");
                barcosNaArea = 0;
            }
            else {
                JOptionPane.showMessageDialog(this, "HÁ EMBARCAÇÕES NESSA AREA!");
                barcosNaArea = 0;
            }
        }
        
        if( recursos == 0){
            JOptionPane.showMessageDialog(this, "VÔCE PERDEU, SEUS RECURSOS ACABARAM.");
        }
    }
}