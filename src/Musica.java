
import java.applet.Applet;
import java.applet.AudioClip;
import java.net.URL;

public class Musica extends Thread{
  
    Musica(String musica) {
        URL url = getClass().getResource(musica+".wav");
        AudioClip audio = Applet.newAudioClip(url);
        audio.play();
        start();
    }
    public void run(){}
    
}
